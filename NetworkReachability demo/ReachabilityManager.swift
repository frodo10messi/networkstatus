//
//  ReachabilityManager.swift
//  NetworkReachability demo
//
//  Created by SGI-Mac7 on 10/11/2018.
//  Copyright © 2018 Slash Global. All rights reserved.
//

import Foundation
import  Reachability
class ReachabilityManager : NSObject{
    static let shared = ReachabilityManager()
    //reachability instance for network status monitoring
    let reachability = Reachability()!
    
    //Boolean to track network reachability
    var isNetworkAvailable : Bool{
        return reachabilityStatus != .none
    }
    //tracks current network status
    var reachabilityStatus : Reachability.Connection = .none
    
    //reachability instance continuously fires note whenever there is a change in the network reachability status
    // this notification will contain the reachability instance as an object
    @objc func reachabilityChanged(note:Notification){
        let reachability = note.object as! Reachability
        
        switch reachability.connection {

        
        case .none:
            print(" not reachable ")
        case .wifi:
            print("reachable via wifi")
        case .cellular:
            print("raechable via cellular")
        }
    }
    func startMonitoring(){
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        
        do {
            // Start the network status notifier
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    func stopMonitoring(){
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self, name:.reachabilityChanged, object: reachability)
    }
    
}
